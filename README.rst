HydrOffice BAG
==============

.. image:: https://img.shields.io/pypi/v/hydroffice.bag.svg
    :target: https://pypi.python.org/pypi/hydroffice.bag
    :alt: PyPi version

.. image:: https://img.shields.io/badge/docs-stable-brightgreen.svg
    :target: http://giumas.github.io/hyo_bag/stable
    :alt: Stable Documentation

.. image:: https://img.shields.io/badge/docs-latest-brightgreen.svg
    :target: http://giumas.github.io/hyo_bag/latest
    :alt: Latest Documentation

.. image:: https://ci.appveyor.com/api/projects/status/thng6eg4g05s8mi4?svg=true
    :target: https://ci.appveyor.com/project/giumas/hyo-bag
    :alt: AppVeyor Status

.. image:: https://travis-ci.org/hydroffice/hyo_bag.svg?branch=master
    :target: https://travis-ci.org/hydroffice/hyo_bag
    :alt: Travis-CI Status



General Info
------------

.. image:: https://bitbucket.org/ccomjhc/hyo_bag/raw/tip/hydroffice/bag/media/favicon.png
    :alt: logo

HydrOffice is a research development environment for ocean mapping. It provides a collection of hydro-packages, each of them dealing with a specific issue of the field.
The main goal is to speed up both algorithms testing and research-2-operation.

The BAG hydro-package collects tools for working with BAG files. BAG is a data format by the `ONS-WG <http://www.opennavsurf.org/>`_ (Open Navigation Surface Working Group).


Dependencies
------------

For the BAG library, you will need:

* ``python`` *[>=2.7, >=3.4]*
* ``numpy``
* ``h5py``
* ``lxml``
* ``gdal`` *[<2.0]*
* ``PyInstaller`` *[for freezing the tools]*

For running some of the example scripts, you might also need:

* ``matplotlib``


Other info
----------

* Bitbucket: `https://bitbucket.org/ccomjhc/hyo_bag <https://bitbucket.org/ccomjhc/hyo_bag>`_
* Project page: `http://www.hydroffice.org <http://www.hydroffice.org>`_
* License: LGPLv3 license (See `LICENSE <https://bitbucket.org/ccomjhc/hyo_bag/raw/tip/LICENSE>`_)
