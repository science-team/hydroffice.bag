How to install
==============

* the simplest way: ``pip install hydroffice.bag``

* for the latest features: ``hg clone ssh://hg@bitbucket.org/gmasetti/hyo_bag``