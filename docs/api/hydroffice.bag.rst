hydroffice.bag package
======================

Subpackages
-----------

.. toctree::

    hydroffice.bag.tools

Submodules
----------

hydroffice.bag.bag module
-------------------------

.. automodule:: hydroffice.bag.bag
    :members:
    :undoc-members:
    :show-inheritance:

hydroffice.bag.base module
--------------------------

.. automodule:: hydroffice.bag.base
    :members:
    :undoc-members:
    :show-inheritance:

hydroffice.bag.bbox module
--------------------------

.. automodule:: hydroffice.bag.bbox
    :members:
    :undoc-members:
    :show-inheritance:

hydroffice.bag.elevation module
-------------------------------

.. automodule:: hydroffice.bag.elevation
    :members:
    :undoc-members:
    :show-inheritance:

hydroffice.bag.helper module
----------------------------

.. automodule:: hydroffice.bag.helper
    :members:
    :undoc-members:
    :show-inheritance:

hydroffice.bag.meta module
--------------------------

.. automodule:: hydroffice.bag.meta
    :members:
    :undoc-members:
    :show-inheritance:

hydroffice.bag.tracklist module
-------------------------------

.. automodule:: hydroffice.bag.tracklist
    :members:
    :undoc-members:
    :show-inheritance:

hydroffice.bag.uncertainty module
---------------------------------

.. automodule:: hydroffice.bag.uncertainty
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: hydroffice.bag
    :members:
    :undoc-members:
    :show-inheritance:
