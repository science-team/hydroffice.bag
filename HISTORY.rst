History
-------

0.2
~~~

2016-05-27

- Added option to read a slice of rows for elevation and uncertainty layers

2016-03-21

- Added option to reproject layers into user-defined EPSG


2015-10-24

- Split BAG Explorer as stand-alone hydro-package


2015-10-09

- Added BAG Explorer
- Started Schematron schema for BAG Metadata profile


0.1
~~~

2015-03-31

- Initial commit.
